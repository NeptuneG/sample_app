# パスについての設定
app_dir = File.expand_path("../..", __FILE__)
shared_dir = "#{app_dir}/shared"
working_directory app_dir

# unicorn稼働設定
worker_processes 2
preload_app true
timeout 30

# 通信用ソケットのパスを指定
listen "#{shared_dir}/sockets/unicorn.sock", :backlog => 64

# ロギング設定
stderr_path "#{shared_dir}/log/unicorn.stderr.log"
stdout_path "#{shared_dir}/log/unicorn.stdout.log"

# master PIDのパスを指定
pid "#{shared_dir}/pids/unicorn.pid"