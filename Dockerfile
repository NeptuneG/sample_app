FROM ruby:2.4.3

RUN apt-get update -qq && \
    apt-get install -y build-essential libpq-dev nodejs && \
    apt-get install -y imagemagick

RUN mkdir -p /sample_app \
             /sample_app/shared/pids \
             /sample_app/shared/sockets \
             /sample_app/shared/log

ADD . /sample_app

WORKDIR /sample_app

RUN bundle install